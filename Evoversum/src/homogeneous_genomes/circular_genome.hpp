#ifndef CIRCULAR_GENOME_HPP
#define CIRCULAR_GENOME_HPP

#include <cstdint>

#include <vector>
#include <map>
#include <iterator>
#include <stdexcept>
#include <limits>

#include <boost/preprocessor/seq.hpp>
#include <boost/preprocessor/repeat.hpp>
#include <boost/foreach.hpp>

#include <utils/smart_struct.hpp>
#include <utils/json_serialization.hpp>
#include <utils/ruint.hpp>

namespace homogeneous_genomes {

		template <class InputIterator, class Distance>
		InputIterator advanced (InputIterator i, Distance n){
			std::advance(i,n);
			return i;
		}

// SMART_STRUCT data with one extra column to specify the function associated with each mutation
#define CIRCULAR_GENOME_MUTATIONS \
    ((std::uint32_t)(gene_deletion)(3)(delete_gene))\
    ((std::uint32_t)(gene_addition)(2)(add_gene))\
    ((std::uint32_t)(gene_transposition)(2)(transpose_gene))

		template <class G>
		class circular_genome {
		public:
			SMART_STRUCT(Mutation_weights,CIRCULAR_GENOME_MUTATIONS)
			typedef Mutation_weights Mutation_parameters;
			typedef std::uint32_t Gene_position;
			typedef G Gene;
		private:
			template <class RNG>
			static Gene_position random_gene_position(RNG& rng){
				static_assert(std::is_same<Gene_position,decltype(rng())>::value,"Aaaaaaa!");
				return rng();
			}
			typedef typename std::map<Gene_position,Gene> Content;
			Content content;
			template <class RNG>
			decltype(Content().end()) random_iterator(RNG& rng){//consider throwing an exception instead of returning end()
				auto cs=content.size();
				return (cs==0)?content.end():advanced(content.begin(),utils::ruint(cs-1,rng));
			}
			template <class RNG>
			void delete_gene(RNG& rng){
				auto it=random_iterator(rng);
				if(it!=content.end())
					content.erase(it);
			}
			template <class RNG>
			void add_gene(RNG& rng){
				content[random_gene_position(rng)]; // in case of collision (very rare) just does nothing
			}
			template <class RNG>
			void transpose_gene(RNG& rng){
				auto new_position=random_gene_position(rng);
				if(content.count(new_position)==1)
					return;
				auto it_old=random_iterator(rng);
				if(it_old!=content.end()){
					content[new_position]=it_old->second;
					content.erase(it_old);
				}
			}
		public:
			//todo: consider implementing genes_in_range(Min,Max)
			template<class RNG>
			void mutate(const Mutation_weights & mw, RNG& rng){
#define HELPER_SUM_TERM(s,instance,a) (std::uint64_t)instance.BOOST_PP_SEQ_ELEM(1,a)+
				auto sum_weights=BOOST_PP_SEQ_FOR_EACH(HELPER_SUM_TERM,mw,CIRCULAR_GENOME_MUTATIONS)0;
				if(!(sum_weights>0))
					throw std::logic_error("genome: weight sum not positive");
				std::int64_t r=utils::ruint(sum_weights-1,rng);
#undef HELPER_SUM_TERM
#define HELPER_MUTATION_CASE(s,instance,a) r-=instance.BOOST_PP_SEQ_ELEM(1,a); if(r<0) {BOOST_PP_SEQ_ELEM(3,a)(rng);return;}
				BOOST_PP_SEQ_FOR_EACH(HELPER_MUTATION_CASE,mw,CIRCULAR_GENOME_MUTATIONS)
#undef HELPER_MUTATION_CASE
					throw std::logic_error("error in genome mutation selection");
			}
			void from_two_genomes(const circular_genome<G>& g1,const circular_genome<G>& g2, Gene_position p1, Gene_position p2){
				if(p2<p1)
					throw std::logic_error("from_two_genomes: p2<p1");
				content.insert(g1.content.begin(),g1.content.lower_bound(p1));
				content.insert(g2.content.lower_bound(p1),g2.content.lower_bound(p2));
				content.insert(g1.content.lower_bound(p2),g1.content.end());
			}
            template <unsigned int FIXED_POINT_POS, class RNG> // todo: check
			void from_two_genomes(const circular_genome<G>& g1,const circular_genome<G>& g2, std::uint32_t g2_weight,RNG& rng){
				static_assert(std::is_unsigned<Gene_position>::value,"from_two_genomes implemented only for unsigned Gene_position");
				static_assert(FIXED_POINT_POS<(sizeof(g2_weight)*8),"from_two_genomes: FIXED_POINT_POS too large");
				enum {GENE_POS_N_BITS=sizeof(Gene_position)*8};
				static_assert(GENE_POS_N_BITS>FIXED_POINT_POS,"from_two_genomes: GENE_POS_N_BITS not greater than FIXED_POINT_POS");
				auto unity=((decltype(g2_weight))1)<<FIXED_POINT_POS;
				if(g2_weight>unity)
					throw std::logic_error("from_two_genomes: g2_weight out of range");
				auto p1=random_gene_position(rng);
				auto p2=p1+(((Gene_position)g2_weight)<<(GENE_POS_N_BITS-FIXED_POINT_POS));
				if((p1<=p2)&&(g2_weight!=unity)){
					from_two_genomes(g1,g2,p1,p2);
				} else {
					from_two_genomes(g2,g1,p2,p1);
				}
			}
			void get_gene_ptrs(std::vector<G*>& out){
				out.clear();
				out.reserve(content.size());
                BOOST_FOREACH(auto& val,content){ // proc tady nemuze byt const auto& ????????
					out.push_back(&(val.second));
				}
			}
			void clear(){
				content.clear();
			}
			void primordial_genome_hack(Gene& in){
				content.clear();
				content[0]=in;
			}
			// todo: implement manipulation interface and move the following out of the class
			void store_to_json(json_spirit::mValue& t) const {
				using namespace json_spirit;
				auto& ta=set_array(t);
				ta.resize(content.size());
				auto ta_it=ta.begin();
				BOOST_FOREACH(auto& item,content){
					auto& json_gene=set_object(*ta_it);
					json_serialization::to_json(item.first,json_gene["gene_position"]);
					json_serialization::to_json(item.second,json_gene["gene_content"]);
					ta_it++;
				}
			}
			void load_from_json(const json_spirit::mValue& s){
				using namespace json_spirit;
				content.clear();
				auto& sa=s.get_array();
				BOOST_FOREACH(auto& item,sa){
					auto& json_gene=item.get_obj();
					auto gene_position=json_serialization::from_json<Gene_position>(json_gene.at("gene_position"));
					if(content.count(gene_position))
						throw std::logic_error("circular_genome: from_json: position already occupied");
					json_serialization::from_json(json_gene.at("gene_content"),content[gene_position]);
				}
			}
		};
}
#endif
