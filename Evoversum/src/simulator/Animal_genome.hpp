#ifndef ANIMAL_GENOME_H
#define ANIMAL_GENOME_H

#include <homogeneous_genomes/circular_genome.hpp>
#include <utils/limited_uint.hpp>
#include "instructions.hpp"
#include "gene.hpp"

namespace Evoversum {

typedef utils::limited_uint<std::uint8_t,N_INSTRUCTIONS-1> Codone;

typedef gene_stuff<Codone> Gene_stuff;

typedef homogeneous_genomes::circular_genome<Gene_stuff::Gene> Animal_genome;

}

#endif
