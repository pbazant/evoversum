#ifndef WORLD_CORE_H_INCLUDED
#define WORLD_CORE_H_INCLUDED

#include <cstdint>

#include <vector>
#include <map>
#include <algorithm>

#include <boost/utility.hpp>
#include <boost/foreach.hpp>

#include <utils/json_serialization.hpp>

#include "Animal_id.hpp"
#include "Dim_exponents.hpp"
#include "exp_torus.hpp"

namespace Evoversum {
	template <class Body_type, class Site_state_type>
	// make it safe with respect to copying // : private boost::noncopyable
	class World_core {
	public:
		typedef Body_type Body;
		typedef Site_state_type Site_state;
	private:
		struct Animal_impl : private boost::noncopyable {
			Body body;
			Place place;
			const Animal_id id;
			Animal_impl(Place p,Animal_id i):place(p),id(i){}
		};
		struct Site_impl {
			Site_impl():state(),animal_impl(){}
			Site_state state;
			Animal_impl* animal_impl;
		};
        exp_torus<Site_impl> map;
		std::map<Animal_id, Animal_impl*> animals; // alternatively, boost.intrusive with advanced lookup functions could be used to store the animals directly, without indirection

	public:
		~World_core() { wipe(); }
        void resize(Dim_exponents es) { wipe(); map.resize(es); }
        Dim_exponents dim_exponents() const {return map.dim_exponents();}
        Dimensions dimensions() const {return map.dimensions();}//convenience function
		void wipe(){
			while(animals.begin()!=animals.end())
				delete_animal(Animal_ref(*animals.begin()->second));
		}
		void swap_animals(Place p1,Place p2){
            auto& s1=map.at(p1); //consider renaming map to terrain
            auto& s2=map.at(p2);
            std::swap(s1.animal_impl,s2.animal_impl);
            if(s1.animal_impl!=nullptr){
                s1.animal_impl->place=map.normalized_place(p1);
			}
            if(s2.animal_impl!=nullptr){
                s2.animal_impl->place=map.normalized_place(p2);
			}
		}
		class Animal_ref {
			friend class World_core;
			explicit Animal_ref(Animal_impl& ai):animal_impl(&ai){}
			Animal_impl* animal_impl;
		public:
			Body& body(){return animal_impl->body;}
			Place place(){return animal_impl->place;}
			Animal_id id(){return animal_impl->id;}
		};

		class Site_ref { // consider renaming to Site_proxy, ditto for Animal_ref
			friend class World_core;
			explicit Site_ref(Site_impl& si):site_impl(&si){}
			Site_impl* site_impl;
		public:
			bool has_animal(){return site_impl->animal_impl!=nullptr;}
            Animal_ref animal_ref(){
				if(!has_animal())
					throw std::runtime_error("Site_ref: animal_ref(): site has no animal");
				return Animal_ref(*(site_impl->animal_impl));
			}
			Site_state& state(){return site_impl->state;}
		};
        Site_ref site_ref(Place p){return Site_ref(map.at(p));}
		// prozatim, nez udelam Site_const_ref
        Site_state site_state(Place p) const {
            return map.at(p).state;
        }

		Animal_ref create_animal(Place place,Animal_id i){
            auto& s=map.at(place);
            if(s.animal_impl!=nullptr)
				throw std::runtime_error("World_core: create_animal: place already contains an animal");
			if(animals.find(i)!=animals.end())
				throw std::runtime_error("World_core: create_animal: id already used by another animal");
            Animal_impl* an=new Animal_impl(map.normalized_place(place),i);
			animals[i]=an;
            s.animal_impl=an;
			return Animal_ref(*an);
		}
        unsigned int n_animals() const {return animals.size();}

        bool animal_exists(Animal_id id) const {
			return animals.count(id)>0;
		}
		Animal_ref animal_ref(Animal_id i) const {
			return Animal_ref(*(animals.at(i)));
		}
		void get_animal_refs(std::vector<Animal_ref>& out) const {
			out.clear();
			out.reserve(animals.size());
            BOOST_FOREACH(auto& el,animals){
                out.push_back(Animal_ref(*(el.second)));
			}
		}
		void delete_animal(Animal_ref an) {
			animals.erase(an.id());
            map.at(an.place()).animal_impl=nullptr;
			delete an.animal_impl;
		}
	};
    template <class Body, class Site_state>
    void animal_ref_to_json(typename World_core<Body,Site_state>::Animal_ref an_ref,json_spirit::mValue& target){
        auto& animal_json=json_spirit::set_object(target);
        json_serialization::to_json(an_ref.body(),animal_json["body"]);
        auto place=an_ref.place(); //todo: investigate, if to_json taking an lvalue ref would eliminate this temporary variable
        json_serialization::to_json(place,animal_json["place"]);
        auto id=an_ref.id();
        json_serialization::to_json(id,animal_json["id"]);
    }
}
//todo: check
namespace json_serialization {
	template <class Body, class Site_state>
	void to_json(const Evoversum::World_core<Body,Site_state>& source,json_spirit::mValue& target){
		using namespace json_spirit;
		using namespace Evoversum;
		auto& target_object=set_object(target);
		auto& world_map_json=set_array(target_object["map"]);
        int X=source.dimensions().x;
        int Y=source.dimensions().y;
		world_map_json.resize(Y);
		for(int y=0;y<Y;y++){
			auto& row=set_array(world_map_json.at(y));
			row.resize(X);
			for(int x=0;x<X;x++){
				auto ss=source.site_state(Place(x,y));
				to_json(ss,row.at(x));
			}
		}
        std::vector<typename World_core<Body,Site_state>::Animal_ref> animal_refs;
		source.get_animal_refs(animal_refs);
		auto& animals_json=set_array(target_object["animals"]);
		animals_json.resize(animal_refs.size());
		for(unsigned int i=0;i<animal_refs.size();i++){
            auto an_ref=animal_refs[i];
            Evoversum::animal_ref_to_json<Body,Site_state>(an_ref,animals_json[i]);
		}
	}
	template <class Body,class Site_state>
	void from_json(const json_spirit::mValue& source,Evoversum::World_core<Body,Site_state>& target)
	{
		using namespace json_spirit;
		using namespace Evoversum;
		auto& source_object=source.get_obj();
		auto& json_map=source_object.at("map").get_array();
        unsigned int Y=json_map.size();
        unsigned int X=json_map.at(0).get_array().size();
        target.resize(exponents_exact(Dimensions(X,Y)));
        for(unsigned int y=0;y<Y;y++){
            for(unsigned int x=0;x<X;x++){
				from_json(json_map.at(y).get_array().at(x),target.site_ref(Place(x,y)).state()); // todo: speed-up using temporaries
			}
		}
		auto& json_animals=source_object.at("animals").get_array();
        BOOST_FOREACH(auto& json_animal,json_animals){
			auto& json_animal_obj=json_animal.get_obj();
				from_json(
					json_animal_obj.at("body"),
					target.create_animal(
						from_json<Place>(json_animal_obj.at("place")),
						from_json<Animal_id>(json_animal_obj.at("id"))
					).body()
				);
		}
	}
}
#endif

