#ifndef WORLD_H_INCLUDED
#define WORLD_H_INCLUDED

#include <cstdint>
#include <cmath>

#include <string>
#include <algorithm>
#include <vector>
#include <limits>

#include <boost/foreach.hpp>

#include <utils/json_serialization.hpp>
#include <utils/smart_struct.hpp>
#include <utils/ruint.hpp>
#include <utils/at_circular.hpp>
#include <utils/shuffle.hpp>

#include "RNG.hpp"
#include "Place.hpp"
#include "Orientation.hpp"
#include "instructions.hpp"
#include "Site_state.hpp"
#include "States.hpp"

#include "World_core.hpp"
#include "Animal_body.hpp"
#include "Parameters.hpp"

#define MAX(a,b) ((a)>=(b)?(a):(b))

namespace Evoversum {

// consider using unsigned int at more places

    typedef World_core<Animal_body,Site_state> World_core2;
		
	enum {IO_VAL_THRESHOLD=16};

	//todo: check!
	//todo: consider using uint64_t !
	std::int64_t think(std::int64_t limit, World_core2::Animal_ref an, World_core2& wc,RNG& rng) { // this function may actually perform slightly more thinking than specified by the limit. the return value is the actual amount of thinking
		auto& ab=an.body();
		auto& io=ab.io;
		auto& memory=ab.memory;
		auto& orientation=ab.orientation;

		std::vector<Gene_stuff::Gene*> genes;
		ab.genome.get_gene_ptrs(genes);
		std::int64_t n_steps_used=0;
		bool finished=false;
		
		while(true){
			utils::shuffle(genes,rng);
			BOOST_FOREACH(auto g_ptr,genes){
				auto g_end=g_ptr->end();
				for(auto it=g_ptr->begin();it!=g_end;it++){
					n_steps_used++;
                    switch(it->get()){
                        INSTRUCTIONS_CASES
                    }
				}
skip_rest_of_gene:
				;
			}
			n_steps_used++; // to avoid an infinite loop if all genes are empty
			if((n_steps_used>=limit)||finished) 
				return n_steps_used;
		}
		throw std::logic_error("internal error in function think()\n");
	}
	//todo: check
    void set_genome_to_primordial(Animal_genome& g){
		int tmp[]=PRIMORDIAL_CODE;
        Gene_stuff::Gene tmp2;
		BOOST_FOREACH(int i,tmp){
			tmp2.push_back(Codone(i));
		}
		g.primordial_genome_hack(tmp2);
	}
//	struct World_format_version { // todo: implement to_json and from_json and add to World to implement version checking
//		enum {MAJOR=3};
//	};

	SMART_STRUCT(Feeding_state,
	    ((int)(phase)(0))
	)
	//todo: check !
	inline void feed(const Feeding_params& fp,Feeding_state& fs, World_core2& wc,RNG& rng){
		int weight_sum=fp.weight_noop+fp.weight_food;
        if (fp.period<=0 || fp.weight_noop<0 || fp.weight_food<0 || weight_sum==0)
			return;
		fs.phase++;
		if (fs.phase < fp.period)
			return;
		fs.phase=0;
        int y_max=wc.dimensions().y+fp.Y2;
        int x_max=wc.dimensions().x+fp.X2;
        for(int y=fp.Y1;y<y_max;y++)
            for(int x=fp.X1;x<x_max;x++){
                if(((int)utils::ruint((unsigned int)weight_sum-1,rng))<fp.weight_food){
                    auto s=wc.site_ref(Place(x,y));
                    if((s.state()==SPACE)&&(fp.food_under_animals||(!s.has_animal())))
                        s.state()=FOOD;
                }
            }
	}

	SMART_STRUCT(World,
		((Parameters)(parameters)())
		((World_core2)(wc)())
		((RNG)(rng)())
		((Feeding_state)(feeding_state)())
		((Animal_id)(new_animal_id)(0))
		((std::uint64_t)(round)(0))
	)
	
	// nezarucim invariant, ze new_animal_id je vetsi nez vsechna prave existujici id, ale nechce se mi prechazet na neco jineho nez smart struct
	inline World_core2::Animal_ref create_animal_with_seq_id(World& w,Place p){
		auto an=w.wc.create_animal(p,w.new_animal_id);
		w.new_animal_id++;
		return an;
	}

	inline void act(World& w,World_core2::Animal_ref an){
		auto & cv=w.parameters.costs_and_values;
		auto& wc=w.wc;
		auto local_site=wc.site_ref(an.place());
		auto front_place=an.place()+displacement(an.body().orientation,1,0);
		auto front_site=wc.site_ref(front_place);
		auto& energy=an.body().energy;
		if(!front_site.has_animal()){
			switch(front_site.state()){
			case SPACE:
				wc.swap_animals(an.place(),front_place);
				energy-=cv.movement_cost;
				break;
			case FOOD:
				front_site.state()=SPACE;
				energy+=cv.food_value;
				wc.swap_animals(an.place(),front_place);
				energy-=cv.movement_cost;
				break;
			case WALL:
				energy-=cv.crash_cost;
				break;
			case DIAMOND:
				if(local_site.state()==SPACE){
					std::swap(front_site.state(),local_site.state());
					energy-=cv.diamond_displacement_cost;
					wc.swap_animals(an.place(),front_place);
					energy-=cv.movement_cost;
				}
				break;
			default:
				throw std::runtime_error("act: unexpected case");
			}
		}
		else {
			auto back_place=an.place()+displacement(an.body().orientation,-1,0);
			auto back_site=wc.site_ref(back_place);
            if(back_site.state()==WALL || back_site.state()==DIAMOND /*||back_site.has_animal()*/){//todo: make the has_animal() condition optional
				auto& front_an_energy=front_site.animal_ref().body().energy;
				if(front_an_energy>0){
					energy+=front_an_energy;
					front_an_energy=0;
				}
			}
            if((back_site.state()==SPACE || back_site.state()==FOOD)&&(!back_site.has_animal())){
				auto io_val_max=std::numeric_limits<Io::value_type>::max();
				auto newborn_energy=an.body().io[IO::RELATIVE_OFFSPRING_INVESTMENT]*an.body().energy/io_val_max;
				auto overall_cost=newborn_energy+cv.replication_cost;
				if(newborn_energy>0 && an.body().energy>overall_cost){
					auto newborn=create_animal_with_seq_id(w,back_place);
					newborn.body().orientation=an.body().orientation;
					auto parent2_weight_par=an.body().io[IO::SECOND_PARENT_GENETIC_INFLUENCE];
                    //parent2_weight_par=32;// temporary hack!! todo: try to find parameters such that xover is advantageous... perhaps...
                    auto parent2_weight=(parent2_weight_par>=0 && parent2_weight_par <=(1<<6))?parent2_weight_par:0;// todo: check !!!
                    newborn.body().genome.from_two_genomes<6>(an.body().genome,front_site.animal_ref().body().genome, parent2_weight, w.rng);
                    auto & mutations=w.parameters.mutations;
                    auto sum_mutation_kind_weight=mutations.genome_mutation_weight+mutations.gene_mutation_weight;
                    auto mut_suppr_par=an.body().io[IO::MUTATION_SUPPRESSION];
                    auto mut_suppr=(mut_suppr_par>=0 && mut_suppr_par <=100)?mut_suppr_par:0;// todo: check !!!
                    if(utils::ruint(0u+mut_suppr,w.rng)<1){
                        if(utils::ruint(sum_mutation_kind_weight-1,w.rng)<mutations.genome_mutation_weight){
                            newborn.body().genome.mutate(mutations.genome_mutation_parameters,w.rng);
                        } else {
                            std::vector<Gene_stuff::Gene*> gene_ptrs;
                            newborn.body().genome.get_gene_ptrs(gene_ptrs);
                            auto s=gene_ptrs.size();
                            if(s!=0){
                                Gene_stuff::mutate(mutations.gene_mutation_parameters,
                                                   *gene_ptrs.at(utils::ruint(s-1u,w.rng)),
                                                   w.rng);
                            }
                        }
                    }
                    newborn.body().energy=newborn_energy;
					energy-=overall_cost;
				}
			}
		}
	}
	inline void perform_turn(World& w,World_core2::Animal_ref an){ // pro jednoduchost zvire muze neco udelat na dluh. Pozor ovsem, aby tim nemohlo pridavat energii do prostredi!!!
		auto & cv=w.parameters.costs_and_values;
		auto & wc=w.wc;
		auto & ab=an.body();
		ab.energy-=cv.existence_cost+ab.energy/cv.inverse_relative_energy_cost;
		auto amount_of_thinking=
			think(ab.energy*
			cv.thinking_cost_denominator/
			cv.thinking_cost_numerator,
			an,wc,w.rng);
		ab.energy-=amount_of_thinking*cv.thinking_cost_numerator/cv.thinking_cost_denominator;
        if(ab.io[IO::ACTION]>=IO_VAL_THRESHOLD)
			act(w, an);
	}

	inline void perform_round(World& w,std::vector<World_core2::Animal_ref>& animal_ref_buffer){ // mozna je ten buffer uplne zbytecny -- zvazit!!!
		auto& wc=w.wc;
		feed(w.parameters.feeding,w.feeding_state,wc,w.rng);
		wc.get_animal_refs(animal_ref_buffer);
		utils::shuffle(animal_ref_buffer,w.rng);
		BOOST_FOREACH(auto an, animal_ref_buffer){
			if(an.body().energy<=0){// nebo naopak?
				wc.delete_animal(an);
			} else {
				perform_turn(w,an);
			}
		}
		w.round++;
	}
	inline World_core2::Animal_ref create_primordial_animal(World& w,Place place){
		auto an=create_animal_with_seq_id(w,place);
		auto& ab=an.body();
		ab.energy=200000;
		set_genome_to_primordial(ab.genome);
		return an;
	}	
}
#endif
