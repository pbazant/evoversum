#ifndef DIM_EXPONENTS_H
#define DIM_EXPONENTS_H

#include "utils/exponent.hpp"

namespace Evoversum {

    struct Dim_exponents {
		Dim_exponents(unsigned int x=0,unsigned int y=0): x(x),y(y) {}
		unsigned int x;
		unsigned int y;
	};
    struct Dimensions {
        Dimensions(unsigned int x=0,unsigned int y=0): x(x),y(y) {}
        unsigned int x;
        unsigned int y;
    };
    inline Dim_exponents exponents_exact(Dimensions dims){
        return Dim_exponents(utils::exponent_exact(dims.x),utils::exponent_exact(dims.y));
    }
    inline Dimensions exponents_to_dimensions(Dim_exponents exps){
        return Dimensions(1<<exps.x,1<<exps.y);
    }

}
#endif
