#include <cstdint>
#include <memory>
#include <vector>
#include <map>
//#include <chrono>
#include <ctime>
#include <utility>

#include <boost/foreach.hpp>

#include "Dim_exponents.hpp"
#include "World.hpp"
#include "World_safe.hpp"

namespace Evoversum {
	class World_safe_pimpl {
	public:
		World w;
	};

	World_safe::World_safe():ws_pimpl(new World_safe_pimpl){}
    World_safe::~World_safe(){}
	boost::signals2::connection World_safe::subscribe_changed(const void_signal::slot_type& subscriber)
	{
		return signal_changed.connect(subscriber);
    }
    //void World_safe::reset(){
    //        ws_pimpl.reset(new World_safe_pimpl);
    //		signal_changed();
    //}
    void World_safe::new_world(Dim_exponents de){
        ws_pimpl.reset(new World_safe_pimpl);
        ws_pimpl->w.wc.resize(de);
        signal_changed();
    }
	void World_safe::perform_round(){
		std::vector<World_core2::Animal_ref> animal_ref_buffer;
		Evoversum::perform_round(ws_pimpl->w,animal_ref_buffer);
		signal_changed();
	}
    void World_safe::evolve_with_time_limit(unsigned int ms){
		std::vector<World_core2::Animal_ref> animal_ref_buffer;
        //typedef std::chrono::high_resolution_clock Clock;
        //typedef std::chrono::milliseconds milliseconds;
        //auto t0 = Clock::now();
		auto t0=clock();
        while (true){
            //auto dt = std::chrono::duration_cast<milliseconds>(Clock::now()-t0).count();
			auto dt=clock()-t0;
            if(dt>(std::int64_t(CLOCKS_PER_SEC)*ms)/1000|| dt<0)
                break;
            Evoversum::perform_round(ws_pimpl->w,animal_ref_buffer);
        }
		signal_changed();
    }
    std::vector<Animal_id> World_safe::create_primordial_animals(std::vector<Place>& places){
        std::vector<Animal_id> result;
        BOOST_FOREACH(auto& p,places){
            result.push_back(Evoversum::create_primordial_animal(ws_pimpl->w,p).id());
        }
		signal_changed();
        return result;
	}
    void World_safe::to_json(json_spirit::mValue& t) const{
        ws_pimpl->w.store_to_json(t);
	}
	void World_safe::from_json(json_spirit::mValue& s){
        ws_pimpl->w.load_from_json(s);
		signal_changed();
	}
    Parameters World_safe::parameters() const{
        return ws_pimpl->w.parameters;
    }
    void World_safe::set_parameters(Parameters p){
        ws_pimpl->w.parameters=p;
		signal_changed();
    }
	void World_safe::rng_seed(std::uint32_t a){
		ws_pimpl->w.rng.seed(a);
		signal_changed();
	}
    std::uint64_t World_safe::round() const{
		return ws_pimpl->w.round;
	}
    //void World_safe::set_round(std::uint64_t i){
    //	ws_pimpl->w.round=i;
    //	signal_changed();
    //}
    Dimensions World_safe::dimensions() const{
		return ws_pimpl->w.wc.dimensions();
	}
    Site_state World_safe::site_state(Place p)const{
        return ws_pimpl->w.wc.site_state(p);
    }
    void World_safe::set_site_states(std::vector<std::pair<Place,Site_state> >& places_states){
        BOOST_FOREACH(auto& a,places_states)
            ws_pimpl->w.wc.site_ref(a.first).state()=a.second;
		signal_changed();
    }
    bool World_safe::site_has_animal(Place p)const{
		return ws_pimpl->w.wc.site_ref(p).has_animal();
	}
    bool World_safe::animal_exists(Animal_id id)const{
        return ws_pimpl->w.wc.animal_exists(id);
    }
    World_safe::Animal_info World_safe::animal_info(Place p)const{
        auto ar=ws_pimpl->w.wc.site_ref(p).animal_ref();
        return Animal_info(ar.id(),ar.place(),ar.body());
    }
    World_safe::Animal_info World_safe::animal_info(Animal_id id)const{
        auto ar=ws_pimpl->w.wc.animal_ref(id);
        return Animal_info(ar.id(),ar.place(),ar.body());
    }
    std::uint64_t World_safe::site_info_partial(Place p) const{
        auto sr=ws_pimpl->w.wc.site_ref(p);
        if(sr.has_animal()){
            auto& ab=sr.animal_ref().body();
            return sr.state()+
                    4+
                    ((ab.orientation.chirality.value()+1)<<2)+
                    (ab.orientation.direction.value()<<4)+
                    (((ab.io[3]>0) && (ab.io[3]<=64))?(1<<6):0);

        } else
        return sr.state();
    }
    unsigned int World_safe::n_animals()const{
		return ws_pimpl->w.wc.n_animals();
	}
    void World_safe::animal_to_json(Place p,json_spirit::mValue& t) const{
        auto ar=ws_pimpl->w.wc.site_ref(p).animal_ref();
        animal_ref_to_json<Animal_body,Site_state>(ar,t);//ugly...
    }
}
