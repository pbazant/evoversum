#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include <boost/preprocessor/seq.hpp>
#include <boost/preprocessor/repeat.hpp>

//todo: v miste (REG0=REG1+REG2;) zvazit, zda nepouzit i reg4

#define REG0 memory[0]
#define REG1 memory[1]
#define REG2 memory[2]
#define REG3 memory[3]

#define INSTRUCTIONS \
(io[IO::ACTION]=50;)\
({auto d=utils::ruint(2u,rng)-1;orientation.direction=chirally_rotated_direction(orientation,d);})\
(io[IO::RELATIVE_OFFSPRING_INVESTMENT]=21;)\
(io[IO::MUTATION_SUPPRESSION]=10;)\
(io[IO::SECOND_PARENT_GENETIC_INFLUENCE]=2;)\
(finished=true;)\
(finished=REG0&1;)\
(finished=REG1&1;)\
(finished=REG2&1;)\
(REG0=-2;)\
(REG0=-1;)\
(REG0=0;)\
(REG0=1;)\
(REG0=2;)\
(REG0=3;)\
(REG0=4;)\
(REG0=8;)\
(REG0=16;)\
(REG0=32;)\
(REG0=64;)\
\
(REG1=-2;)\
(REG1=-1;)\
(REG1=0;)\
(REG1=1;)\
(REG1=2;)\
(REG1=3;)\
(REG1=4;)\
(REG1=8;)\
(REG1=16;)\
(REG1=32;)\
(REG1=64;)\
\
(REG2=-2;)\
(REG2=-1;)\
(REG2=0;)\
(REG2=1;)\
(REG2=2;)\
(REG2=3;)\
(REG2=4;)\
(REG2=8;)\
(REG2=16;)\
(REG2=32;)\
(REG2=64;)\
\
(REG3=-2;)\
(REG3=-1;)\
(REG3=0;)\
(REG3=1;)\
(REG3=2;)\
(REG3=3;)\
(REG3=4;)\
(REG3=8;)\
(REG3=16;)\
(REG3=32;)\
(REG3=64;)\
\
(REG0=utils::at_circular_s(memory,REG1);)\
(REG0=utils::at_circular_s(memory,REG2);)\
(REG0=utils::at_circular_s(memory,REG3);)\
(REG1=utils::at_circular_s(memory,REG0);)\
(REG1=utils::at_circular_s(memory,REG2);)\
(REG1=utils::at_circular_s(memory,REG3);)\
(REG2=utils::at_circular_s(memory,REG0);)\
(REG2=utils::at_circular_s(memory,REG1);)\
(REG2=utils::at_circular_s(memory,REG3);)\
(REG3=utils::at_circular_s(memory,REG0);)\
(REG3=utils::at_circular_s(memory,REG1);)\
(REG3=utils::at_circular_s(memory,REG2);)\
\
(utils::at_circular_s(memory,REG0)=REG1;)\
(utils::at_circular_s(memory,REG0)=REG2;)\
(utils::at_circular_s(memory,REG0)=REG3;)\
(utils::at_circular_s(memory,REG1)=REG0;)\
(utils::at_circular_s(memory,REG1)=REG2;)\
(utils::at_circular_s(memory,REG1)=REG3;)\
(utils::at_circular_s(memory,REG2)=REG0;)\
(utils::at_circular_s(memory,REG2)=REG1;)\
(utils::at_circular_s(memory,REG2)=REG3;)\
(utils::at_circular_s(memory,REG3)=REG0;)\
(utils::at_circular_s(memory,REG3)=REG1;)\
(utils::at_circular_s(memory,REG3)=REG2;)\
\
(REG0=(Animal_memory::value_type) rng();)\
(REG1=(Animal_memory::value_type) rng();)\
(REG2=(Animal_memory::value_type) rng();)\
(REG3=(Animal_memory::value_type) rng();)\
\
(REG0=REG1+REG2;)\
(REG0=REG1-REG2;)\
(REG0=REG1*REG2;)\
(REG0=REG1&REG2;)\
(REG0=REG1^REG2;)\
(REG0=REG1|REG2;)\
(REG0=REG1<<REG2;)\
\
(REG1=REG2+REG0;)\
(REG1=REG2-REG0;)\
(REG1=REG2*REG0;)\
(REG1=REG2&REG0;)\
(REG1=REG2^REG0;)\
(REG1=REG2|REG0;)\
(REG1=REG2<<REG0;)\
\
(REG2=REG1+REG0;)\
(REG2=REG1-REG0;)\
(REG2=REG1*REG0;)\
(REG2=REG1&REG0;)\
(REG2=REG1^REG0;)\
(REG2=REG1|REG0;)\
(REG2=REG1<<REG0;)\
\
(utils::at_circular_s(io,REG0)=REG1;)\
(utils::at_circular_s(io,REG0)=REG2;)\
(utils::at_circular_s(io,REG1)=REG2;)\
(utils::at_circular_s(io,REG1)=REG0;)\
(utils::at_circular_s(io,REG2)=REG0;)\
(utils::at_circular_s(io,REG2)=REG1;)\
\
(REG1=utils::at_circular_s(io,REG0);)\
(REG2=utils::at_circular_s(io,REG0);)\
(REG2=utils::at_circular_s(io,REG1);)\
(REG0=utils::at_circular_s(io,REG1);)\
(REG0=utils::at_circular_s(io,REG2);)\
(REG1=utils::at_circular_s(io,REG2);)\
\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG0,REG1)); REG2=site_ref.state(); REG3=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG0,REG2)); REG1=site_ref.state(); REG3=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG1,REG0)); REG2=site_ref.state(); REG3=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG1,REG2)); REG0=site_ref.state(); REG3=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG2,REG0)); REG1=site_ref.state(); REG3=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG2,REG1)); REG0=site_ref.state(); REG3=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG0,REG1)); REG3=site_ref.state(); REG2=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG0,REG2)); REG3=site_ref.state(); REG1=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG1,REG0)); REG3=site_ref.state(); REG2=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG1,REG2)); REG3=site_ref.state(); REG0=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG2,REG0)); REG3=site_ref.state(); REG1=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
({auto site_ref=wc.site_ref(an.place()+displacement(orientation,REG2,REG1)); REG3=site_ref.state(); REG0=(site_ref.has_animal())?site_ref.animal_ref().body().io[IO::MOOD]|1:0;})\
\
(if(REG0>0) goto skip_rest_of_gene;)\
(if(REG0&1) goto skip_rest_of_gene;)\
(if(REG0==0) goto skip_rest_of_gene;)\
(if(REG1>0) goto skip_rest_of_gene;)\
(if(REG1&1) goto skip_rest_of_gene;)\
(if(REG1==0) goto skip_rest_of_gene;)\
(if(REG2>0) goto skip_rest_of_gene;)\
(if(REG2&1) goto skip_rest_of_gene;)\
(if(REG2==0) goto skip_rest_of_gene;)\
(if(REG3>0) goto skip_rest_of_gene;)\
(if(REG3&1) goto skip_rest_of_gene;)\
(if(REG3==0) goto skip_rest_of_gene;)\
\
(if(REG0>0) orientation.chirality.flip();)\
(if(REG1>0) orientation.chirality.flip();)\
(if(REG2>0) orientation.chirality.flip();)\
(if(REG3>0) orientation.chirality.flip();)\
\
(orientation.direction=chirally_rotated_direction(orientation,REG0);)\
(orientation.direction=chirally_rotated_direction(orientation,REG1);)\
(orientation.direction=chirally_rotated_direction(orientation,REG2);)\
(orientation.direction=chirally_rotated_direction(orientation,REG0);)\
(orientation.direction=chirally_rotated_direction(orientation,REG1);)\
(orientation.direction=chirally_rotated_direction(orientation,REG2);)

//(compress_value(energy,REG0);)
//(compress_value(energy,REG1);)
//(compress_value(energy,REG2);)
//(compress_value(energy,REG3);)

namespace Evoversum {
enum{N_INSTRUCTIONS=BOOST_PP_SEQ_SIZE(INSTRUCTIONS)};
}

#define INSTRUCTIONS_CASES_HELPER(s,n,a) case n: BOOST_PP_SEQ_ELEM(n,a) break;
#define INSTRUCTIONS_CASES BOOST_PP_REPEAT(BOOST_PP_SEQ_SIZE(INSTRUCTIONS),INSTRUCTIONS_CASES_HELPER,INSTRUCTIONS)

//#define PRIMORDIAL_CODE {0,1,2,3,4,5}
#define PRIMORDIAL_CODE {0,1,2,3,5}

#endif
