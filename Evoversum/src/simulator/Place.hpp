#ifndef PLACE_H
#define PLACE_H
#include <utils/smart_struct.hpp>

namespace Evoversum {

    SMART_STRUCT(Place,((unsigned int)(x)(0))((unsigned int)(y)(0)))
    //
    struct Displacement {
        Displacement(int x=0, int y=0): x(x),y(y){}
        int x;
        int y;
	};
	inline Displacement operator+ (Displacement d1, Displacement d2)
	{   return Displacement(d1.x+d2.x,d1.y+d2.y);}

	inline Displacement operator* (int k,Displacement d)
	{   return Displacement(d.x*k,d.y*k);}

	inline Place operator+ (Place p, Displacement d)
	{   return Place(p.x+d.x,p.y+d.y);}

	inline void operator += (Place& p, Displacement d)
    {   p.x+=d.x; p.y+=d.y;}

}
#endif
