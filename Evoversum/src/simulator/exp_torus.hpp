#ifndef EXP_TORUS_H
#define EXP_TORUS_H

#include <vector>
#include <stdexcept>

#include "Place.hpp"
#include "Dim_exponents.hpp"

namespace Evoversum {

    template <class T>
    class exp_torus {
	protected:
        Dim_exponents exponents;
        typedef std::vector<T> Vec;
        Vec b;
        typename Vec::size_type mask_x, mask_y;
	public:
        exp_torus(Dim_exponents es=Dim_exponents())
        {  resize(es);
		}
        Place normalized_place(Place p) const {
            return Place(p.x&mask_x,p.y&mask_y);
        }
        void resize(Dim_exponents es){
            auto n_bits=sizeof(typename Vec::size_type)*8;
            auto sumexp=es.x+es.y;
            if (sumexp<es.x||sumexp>=n_bits)
                throw std::runtime_error("exp_torus: resize: dimensions too large");
            b.clear();
            b.resize(1<<sumexp);
            mask_x=(1<<es.x)-1;
            mask_y=(1<<es.y)-1;
            exponents=es;
		}
        Dim_exponents dim_exponents() const
        {   return exponents;
        }
        Dimensions dimensions() const {
            return exponents_to_dimensions(exponents);
        }
        T& at(Place p){
            auto np=normalized_place(p);
            return b[(np.y<<exponents.x)+np.x];
        }
        const T& at(Place p) const{
            auto np=normalized_place(p);
            return b[(np.y<<exponents.x)+np.x];
        }
    };
}
#endif
