#ifndef RNG_H_INCLUDED
#define RNG_H_INCLUDED

#include <cstdint>

#include <algorithm>

#include <boost/random.hpp>
#include <boost/lexical_cast.hpp>

#include <utils/json_spirit_convenience.hpp>

namespace Evoversum {
	typedef boost::taus88 RNG; // source of randomness -- very fast!
	//typedef std::mt19937 RNG;
}
namespace json_serialization {
	inline void to_json(const Evoversum::RNG& source,json_spirit::mValue& target) {
		target=boost::lexical_cast<std::string>(source);
	}
	inline void from_json(const json_spirit::mValue& source,Evoversum::RNG& target) {
		target=boost::lexical_cast<Evoversum::RNG>(source.get_str());
	}
}
#endif
