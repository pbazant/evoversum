#ifndef SITE_STATE_H
#define SITE_STATE_H

#include <cstdint>

namespace Evoversum{

typedef std::uint32_t Site_state;

}

#endif
