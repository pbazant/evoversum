#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <cstdint>

#include <utils/json_serialization.hpp>
#include <utils/smart_struct.hpp>
#include "Animal_genome.hpp"

namespace Evoversum {

SMART_STRUCT(Feeding_params,
    ((int)(period)(10))
    ((int)(X1)(0))
    ((int)(Y1)(0))
    ((int)(X2)(0))
    ((int)(Y2)(0))
    ((bool)(food_under_animals)(false))
    ((int)(weight_noop)(97))
    ((int)(weight_food)(3))
)

SMART_STRUCT(Costs_and_values,
    ((int)(existence_cost)(10))
    ((int)(inverse_relative_energy_cost)(20000))
    ((int)(thinking_cost_numerator)(1))
    ((int)(thinking_cost_denominator)(10))
    ((int)(movement_cost)(100))
    ((int)(diamond_displacement_cost)(-200))
    ((int)(crash_cost)(10000))
    ((int)(replication_cost)(200))
    ((int)(food_value)(1000))
)

SMART_STRUCT(Mutation_parameters,
	((Gene_stuff::Mutation_parameters)(gene_mutation_parameters)())
    ((Animal_genome::Mutation_parameters)(genome_mutation_parameters)())
	((std::uint32_t)(genome_mutation_weight)(1))
    ((std::uint32_t)(gene_mutation_weight)(25))
)

SMART_STRUCT(Parameters,
	((Feeding_params)(feeding)())
	((Costs_and_values)(costs_and_values)())
	((Mutation_parameters)(mutations)())
)


}

#endif
