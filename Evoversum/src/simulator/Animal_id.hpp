#ifndef ANIMAL_ID_H
#define ANIMAL_ID_H

#include <cstdint>

namespace Evoversum {

typedef std::uint64_t Animal_id;

}

#endif
