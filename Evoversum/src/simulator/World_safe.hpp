#ifndef WORLD_SAFE_HPP
#define WORLD_SAFE_HPP

#include <cstdint>
#include <memory>

#include <boost/noncopyable.hpp>
#include <boost/signals2/signal.hpp>

#include <utils/json_spirit_convenience.hpp>

#include "Place.hpp"
#include "Site_state.hpp"
#include "Animal_id.hpp"
#include "Animal_body.hpp"
#include "Parameters.hpp"
#include "States.hpp"
#include "Dim_exponents.hpp"

namespace Evoversum {

	class World_safe_pimpl;

	typedef boost::signals2::signal<void ()> void_signal;

	class World_safe: boost::noncopyable {
		std::unique_ptr<World_safe_pimpl> ws_pimpl;
        void_signal signal_changed;
    public:
		World_safe();
        ~World_safe();
		boost::signals2::connection subscribe_changed(const void_signal::slot_type& subscriber);
        //void reset();
        void new_world(Dim_exponents de);
		void perform_round();
        void evolve_with_time_limit(unsigned int ms);
        std::vector<Animal_id> create_primordial_animals(std::vector<Place>& places);
        void to_json(json_spirit::mValue& t) const;
		void from_json(json_spirit::mValue& s);
        Parameters parameters() const;
        void set_parameters(Parameters p);
		void rng_seed(std::uint32_t a);
        std::uint64_t round() const;
        //void set_round(std::uint64_t i);
        Dimensions dimensions() const;
		//v client server verzi misto toho site_states!!!
        Site_state site_state(Place p) const;
        void set_site_states(std::vector<std::pair<Place,Site_state> >& places_states);
        bool site_has_animal(Place p) const;
        bool animal_exists(Animal_id id) const;
        struct Animal_info {
            Animal_info(Animal_id id,Place place,Animal_body& animal_body):id(id),place(place),animal_body(animal_body){}
            Animal_id id;
            Place place;
            Animal_body animal_body;
        };
        Animal_info animal_info(Place p) const;
        Animal_info animal_info(Animal_id id) const;
        std::uint64_t site_info_partial(Place p) const;
        void animal_to_json(Place p,json_spirit::mValue& t) const;
        unsigned int n_animals() const;
	};
}

#endif
