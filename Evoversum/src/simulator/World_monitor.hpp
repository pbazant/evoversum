#ifndef WORLD_MONITOR_HPP
#define WORLD_MONITOR_HPP

#include <cstdint>
#include <memory>
#include <mutex>

//#include <boost/noncopyable.hpp>

#include <utils/json_spirit_convenience.hpp>

#include "Place.hpp"
#include "Site_state.hpp"
#include "Animal_id.hpp"
#include "Animal_body.hpp"
#include "Parameters.hpp"
#include "States.hpp"
#include "Dim_exponents.hpp"
#include "World_safe.hpp"

......
namespace Evoversum { 

    typedef std::lock_guard<std::mutex> lg;
    typedef std::unique_lock<std::mutex> ul;
	
	class World_monitor { 
		World_safe ws;
        mutable std::mutex mtx;
	public:
		void reset(){lg l(mtx);ws.reset();}
		void perform_round(){lg l(mtx);ws.perform_round();}
		void create_primordial_animal(Place p){lg l(mtx);ws.create_primordial_animal(p);}
        void to_json(json_spirit::mValue& t) const{lg l(mtx);...}
		void from_json(json_spirit::mValue& s){lg l(mtx);}
        Parameters parameters() const{lg l(mtx);}
        void set_parameters(Parameters p){lg l(mtx);}
		void rng_seed(std::uint32_t a){lg l(mtx);}
        std::uint64_t round() const{lg l(mtx);}
		void set_round(std::uint64_t i){lg l(mtx);}
        Dimensions dimensions() const{lg l(mtx);}
        void set_dim_exponents(Dim_exponents de){lg l(mtx);}
        Site_state site_state(Place p) const{lg l(mtx);}
        void set_site_state( Place p, Site_state state){lg l(mtx);}
        bool site_has_animal(Place p) const{lg l(mtx);}
        bool animal_exists(Animal_id id) const{lg l(mtx);}
        World_safe::Animal_info animal_info(Place p) const{lg l(mtx);}
        World_safe::Animal_info animal_info(Animal_id id) const{lg l(mtx);}
        void animal_to_json(Place p,json_spirit::mValue& t) const{lg l(mtx);}
        unsigned int n_animals() const{lg l(mtx);}

        template <class InputMultiArray>//a model of MultiArray concept
        void load_from_pixmap(InputMultiArray& a){
            lg l(mtx);
	    ws.
        }
	};
}

#endif
