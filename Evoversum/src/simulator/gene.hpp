#ifndef GENE_H_INCLUDED
#define GENE_H_INCLUDED

#include <cstdint>

#include <vector>
#include <map>
#include <iterator>
#include <stdexcept>
#include <limits>

#include <boost/preprocessor/seq.hpp>
#include <boost/preprocessor/repeat.hpp>

#include <utils/smart_struct.hpp>
#include <utils/json_serialization.hpp>
#include <utils/ruint.hpp>

#include "RNG.hpp"
#include "instructions.hpp"

namespace Evoversum {

// SMART_STRUCT data with one extra column to specify the code associated with the mutation
// (no problem with the last column -- SMART_STRUCT just ignores it)
#define GENE_MUTATIONS \
	((std::uint32_t)(codone_insertion)(5)(insert_codone))\
	((std::uint32_t)(codone_deletion)(13)(delete_codone))\
	((std::uint32_t)(codone_duplication)(5)(duplicate_codone))\
	((std::uint32_t)(codone_swap)(5)(swap_codones))\
	((std::uint32_t)(codone_randomization)(3)(randomize_codone))

		template <class C>
		class gene_stuff { //todo: zvazit, zda to neni prekombinovane
		public:
			SMART_STRUCT(Mutation_weights,GENE_MUTATIONS)
			typedef Mutation_weights Mutation_parameters;
			typedef std::vector<C> Gene;
		private:
			static void insert_codone(Gene& vec,RNG& rng){
				int p=utils::ruint(vec.size(),rng);
				vec.insert(vec.begin()+p,C());
                //todo: make the randomization reusable (sth like vec[p]=randomized<C>(Rng& rng)?)
                vec[p].set(utils::ruint((unsigned int)(N_INSTRUCTIONS-1),rng));
			}
			static void delete_codone(Gene& vec, RNG& rng){
				if(vec.size()==0)
					return;
				int p=utils::ruint(vec.size()-1,rng);
				vec.erase(vec.begin()+p);
			}
			static void duplicate_codone(Gene& vec, RNG& rng){
				if(vec.size()<1)
					return;
				int p=utils::ruint(vec.size(),rng);
				int q=utils::ruint(vec.size()-1,rng);
				C tmp=vec[q];
				vec.insert(vec.begin()+p,tmp);
			}
			static void swap_codones(Gene& vec, RNG& rng){
				if(vec.size()<2)
					return;
				int p=utils::ruint(vec.size()-2,rng);
				C tmp=vec[p];
				vec[p]=vec[p+1];
				vec[p+1]=tmp;
			}
			static void randomize_codone(Gene& vec,RNG& rng){
				if(vec.size()==0)
					return;
				int p=utils::ruint(vec.size()-1,rng);
                vec[p].set(utils::ruint((unsigned int)(N_INSTRUCTIONS-1),rng));// see comment at insert_codone
			}
			// static void from_two_vecs(const Gene& v1, const Gene& v2, Gene& vec, int g1_weight, int g2_weight, RNG& rng) {...} // double xover at the codone level
		public:
			static void mutate(const Mutation_weights& weights, Gene& vec, RNG& rng) {
#define HELPER_SUM_TERM(s,instance,a) (std::uint64_t)instance.BOOST_PP_SEQ_ELEM(1,a)+
				auto sum_weights=BOOST_PP_SEQ_FOR_EACH(HELPER_SUM_TERM,weights,GENE_MUTATIONS)0;
				if(!(sum_weights>0))
					throw std::logic_error("gene: weight sum not positive");
				std::int64_t r=utils::ruint(sum_weights-1,rng);//0 <= r < sum of weights
#undef HELPER_SUM_TERM
#define HELPER_MUTATION_CASE(s,instance,a) r-=instance.BOOST_PP_SEQ_ELEM(1,a); if(r<0) {BOOST_PP_SEQ_ELEM(3,a)(vec,rng);return;}
				BOOST_PP_SEQ_FOR_EACH(HELPER_MUTATION_CASE,weights,GENE_MUTATIONS)
#undef HELPER_MUTATION_CASE
				throw std::logic_error("error in gene mutation selection");
			}
		};
}
#endif
