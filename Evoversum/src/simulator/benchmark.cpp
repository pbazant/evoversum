#include <cstdint>
#include <ctime>

#include "World_safe.hpp"
#include "RNG.hpp"

using namespace Evoversum;

int main(int argc, const char* argv[]){
	if(argc!=2)
		return -1;
	int n_rounds = atoi(argv[1]);
    Evoversum::World_safe w;
    w.new_world(Dim_exponents(7,7));
    //Animal_id aid=
	std::vector<Place> places;
	places.push_back(Place(1,1));
	places.push_back(Place(2,1));
	places.push_back(Place(1,2));
	places.push_back(Place(2,2));
	w.create_primordial_animals(places);
	auto t0=clock();
    for(int i=0;i<n_rounds;i++){
        w.perform_round();
    }
	auto t1=clock();
	auto s=(t1-t0)/CLOCKS_PER_SEC;
	std::cout << s << " s\n";
    std::cout << w.round() << " rounds\n";
    std::cout << w.n_animals() << " animals\n"<< std::flush;
}
