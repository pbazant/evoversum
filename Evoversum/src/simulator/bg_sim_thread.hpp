......
template<class C> // C has to be a monitor !!!!!!
class background_simulation_thread {
std::thread t;
C c;
enum {Stopped=0,Running=1,Terminating=2};
notifying_variable<int> state;
static void simulate(C* c, notifying_variable<int>* s){
while(true){
switch(s->get()){
case Stopped: s->wait();break; // wait and get?? think twice!
case Running: c->perform_round();break;
case Terminating: return;
}
}
}
public:
background_simulation_thread(){
t=std::thread(&simulate,&c,&state);
}
~background_simulation_thread(){
state.set(Terminating);
t.join();
}
void set_running(bool b){
state.set(b?(int)Running:(int)Stopped);
}
bool is_running(){
return state.get()==Running; // The state Terminating is used only during destruction
}
C& sim(){return c;}
};
