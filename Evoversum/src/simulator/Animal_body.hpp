#ifndef ANIMAL_BODY_H
#define ANIMAL_BODY_H

#include <cstdint>

#include <utils/json_serialization.hpp>
#include <utils/smart_struct.hpp>
#include <utils/ZI_array.hpp>
#include <utils/ZI_array_json.hpp>

#include "Orientation.hpp"

#include "Animal_genome.hpp"

namespace Evoversum {

namespace IO {
    enum {
        ACTION,
        RELATIVE_OFFSPRING_INVESTMENT,
        MUTATION_SUPPRESSION,
        SECOND_PARENT_GENETIC_INFLUENCE,
        MOOD,
        RESERVED0, // perhaps telepathy_input
        RESERVED1,
        RESERVED2,
        N_IO_REGS // try to keep it a power of two for faster modulo
    };
}
typedef utils::ZI_array<std::int8_t,IO::N_IO_REGS> Io;
typedef utils::ZI_array<Io::value_type,64> Animal_memory; // jine implementace mozku mohou pouzit neco jineho nez Io::value_type

SMART_STRUCT(Animal_body,
    ((Orientation<4>)(orientation)())
    ((Io)(io)()) // wishes, mood, and in the future "messages" from other animals
    ((Animal_memory)(memory)())
    ((Animal_genome)(genome)())
    ((std::int64_t)(energy)(0))
)

}

#endif
