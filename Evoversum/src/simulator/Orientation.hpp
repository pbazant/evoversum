#ifndef ORIENTATION_H_INCLUDED
#define ORIENTATION_H_INCLUDED

#include "Place.hpp"

#include <utils/json_spirit_convenience.hpp>
#include <utils/mod.hpp>

namespace Evoversum {
	template <int N>
	class Direction {
	private:
		int d;
	public:
		Direction(int i=0) {set(i);}
        void set(int i) {d=utils::mod(i,N);}
		int value() const {return d;}
	};
	template<int N>
	Direction<N> rotated(Direction<N> d,int amount) {return Direction<N>(d.value()+amount);}
	//   1
	// 2 x 0
	//   3
    inline Displacement unit_displacement(Direction<4> d){
		const static Displacement ds[]={Displacement(1,0),Displacement(0,-1),Displacement(-1,0),Displacement(0,1)};
		return ds[d.value()];
	}
	////   2 1
	//// 3 x 0
	//// 4 5
	//Displacement displacement(Direction<6>& d){
	//static const Displacement ds[]=
	//{Displacement(1,0),Displacement(1,-1),Displacement(0,-1),
	// Displacement(-1,0),Displacement(-1,1),Displacement(0,1)};
	//}

	class Chirality
	{private:
	int c;
	public:
		Chirality(int i=1) : c(((i+1)&2)-1) {}
		int value() const {return c;}
		void set(int i) {c=((i+1)&2)-1;}
		void flip() {c=-c;}
	};

	template<int N>
	class Orientation {
	public:
		typedef Direction<N> D;
		D direction;
		Chirality chirality;
		Orientation(){}
		//Orientation(const D& d, const Chirality& c):direction(d), chirality(c){}
	};

	template <int N>
	Direction<N> chirally_rotated_direction(Orientation<N> o,int amount){return rotated(o.direction,amount*o.chirality.value());}

	template <int N>
	Displacement displacement(Orientation<N> o,int d, int s)
	{
		return d*unit_displacement(o.direction)+abs(s)*unit_displacement(chirally_rotated_direction(o,s>0?1:-1)); // this should nicely work with hex grids too!!!
	}
}
namespace json_serialization {
	template <int N>
	inline void to_json(const Evoversum::Orientation<N>& s,json_spirit::mValue& t){
		auto& json_object=json_spirit::set_object(t);
		json_object["chirality"]=s.chirality.value();
		json_object["direction"]=s.direction.value();
	}
	template <int N>
	inline void from_json(const json_spirit::mValue& s,Evoversum::Orientation<N>& t){
		auto& obj=s.get_obj();
		t.chirality.set(obj.at("chirality").get_int());
		t.direction.set(obj.at("direction").get_int());
	}
}
#endif
