#ifndef ZI_ARRAY_JSON_H
#define ZI_ARRAY_JSON_H

#include "ZI_array.hpp"

#include "json_spirit_convenience.hpp"

namespace json_serialization {

    template <class C,size_t N>
	inline void to_json(const utils::ZI_array<C,N>& s,json_spirit::mValue& t){
        to_json(*((std::array<C,N>*)&s),t);
	}
    template <class C,size_t N>
	inline void from_json(const json_spirit::mValue& s, utils::ZI_array<C,N>& t){
        from_json(s,*((std::array<C,N>*)&t));
	}
}

#endif