#ifndef AT_CIRCULAR_HPP
#define AT_CIRCULAR_HPP

#include "mod.hpp"

namespace utils {

//signed wrap-around access to std::arrays and their ilk
//BUG: if size() is tooooooo big, conversion to signed int will cause trouble!
template <class T>
typename T::value_type& at_circular_s(T& a,int i){
	return a[mod(i,(int)a.size())];
}
//unsigned wrap-around access to std::arrays and their ilk
template <class T>
typename T::value_type& at_circular_u(T& a,typename T::size_type i){
	return a[i%a.size()];
}

}
#endif
