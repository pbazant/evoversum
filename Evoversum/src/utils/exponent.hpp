#ifndef UTILS_EXPONENT_HPP
#define UTILS_EXPONENT_HPP

#include <stdexcept>

namespace utils {

inline unsigned int exponent_exact(unsigned int val){
    unsigned int exp=0;
    while(true){
        unsigned int val2=1<<exp;
        if(val2==0)
            throw std::logic_error("exponent_exact: value is not a power of two");
        if(val2==val)
            return exp;
        exp++;
    }
}

}

#endif
