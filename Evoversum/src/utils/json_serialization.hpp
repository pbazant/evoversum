#ifndef JSON_SERIALIZATION
#define JSON_SERIALIZATION

#include <cstdint>

#include <array>

#include <boost/preprocessor/seq.hpp>

#include "json_spirit_convenience.hpp"

namespace json_serialization {
	template <class C>
	inline void to_json(const C& s, json_spirit::mValue& t){
		s.store_to_json(t);
	}
	template <class C>
	inline void from_json(const json_spirit::mValue& s, C& t){
		t.load_from_json(s);
	}


// todo: double-check for signedness issues

//todo: consider removing boost preprocessor dependence
#define TO_JSON_FROM_JSON_BASIC(r,_,type) \
	inline void to_json(const type& s,json_spirit::mValue& t){t=s;}\
	inline void from_json(const json_spirit::mValue& s,type& t){t=s.get_value<type>();}
	BOOST_PP_SEQ_FOR_EACH(TO_JSON_FROM_JSON_BASIC,_,(bool)(int)(std::uint64_t)(std::int64_t))

	//todo: check for range!
	inline void to_json(const std::int8_t& s,json_spirit::mValue& t){t=(int)s;}
	inline void from_json(const json_spirit::mValue& s,std::int8_t& t){t=s.get_value<int>();}
	inline void to_json(const std::uint8_t& s,json_spirit::mValue& t){t=(int)s;}
	inline void from_json(const json_spirit::mValue& s,std::uint8_t& t){t=s.get_value<int>();}
	inline void to_json(const std::uint32_t& s,json_spirit::mValue& t){t=(std::uint64_t)s;}
	inline void from_json(const json_spirit::mValue& s,std::uint32_t& t){t=(std::uint32_t)s.get_value<std::uint64_t>();}//todo: check for overflow

    template <class C>
    inline void seq_to_json(const C& s,json_spirit::mValue& t){ // todo: rewrite without random access
        auto& json_array=json_spirit::set_array(t);
        json_array.resize(s.size());
        for(unsigned int i=0;i<s.size();i++){
            to_json(s[i],json_array[i]);
        }
    }
    template <class C>
	inline void vector_like_from_json(const json_spirit::mValue& s,C& t){ // anything clearable, resizable and random-accessible (todo: rewrite without random access)
		auto& json_array=s.get_array();
		t.clear();
		t.resize(json_array.size());
		for(unsigned int i=0;i<json_array.size();i++){
			from_json(json_array[i],t[i]);
		}
	}
	template <class C>
	inline void to_json(const std::vector<C>& s,json_spirit::mValue& t){
		seq_to_json(s,t);
	}
	template <class C>
	inline void from_json(const json_spirit::mValue& s,std::vector<C>& t){
		vector_like_from_json(s,t);
	}
    template <class C,size_t N>
	inline void to_json(const std::array<C,N>& s,json_spirit::mValue& t){
		seq_to_json(s,t);
	}
    template <class C,size_t N>
    inline void from_json(const json_spirit::mValue& s, std::array<C,N>& t){
        auto& json_array=s.get_array();
        if(json_array.size()!=t.size())
            throw std::runtime_error("from_json: json array size does not match array size");
        for(unsigned int i=0;i<t.size();i++){
            from_json(json_array[i],t[i]);
        }
    }
    //convenience functions:
	template <class C>
	inline C from_json(const json_spirit::mValue& source){ // inefficient for large data
		C tmp;
		from_json(source,tmp);
		return tmp;
	}
	template <class C>
	inline json_spirit::mValue to_json(const C& source){
		json_spirit::mValue tmp;
		to_json(source,tmp);
		return tmp;
	}
}
#endif
