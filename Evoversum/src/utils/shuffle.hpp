#ifndef UTILS_SHUFFLE_HPP
#define UTILS_SHUFFLE_HPP

#include <algorithm>

#include "ruint.hpp"

namespace utils {
	template<class Array_like, class RNG>
	void shuffle(Array_like& a,RNG& rng){
		for(auto s=a.size();s>1;s--){
			std::swap(a[ruint(s-1,rng)],a[s-1]);
		}
	}
}

#endif