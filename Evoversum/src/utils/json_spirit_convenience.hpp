#ifndef JSON_SPIRIT_CONVENIENCE_HPP
#define JSON_SPIRIT_CONVENIENCE_HPP

#include <json_spirit_reader_template.h>
#include <json_spirit_writer_template.h>
#include <json_spirit_value.h>
//#include <json_spirit_stream_reader.h>

namespace json_spirit
{
	inline mArray& set_array(mValue& val) {
		val=mArray();return val.get_array();
	}
	inline mObject& set_object(mValue& val) {
		val=mObject();return val.get_obj();
	}
}


#endif