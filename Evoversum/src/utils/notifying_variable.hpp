#ifndef NOTIFYING_VARIABLE_H
#define NOTIFYING_VARIABLE_H

#include <mutex>
#include <condition_variable>

namespace utils {

	template <class A>
	class notifying_variable {
		std::mutex m;
		std::condition_variable cv;
		A var;
		typedef std::unique_lock<std::mutex> ul;
    public:
		notifying_variable():var(){}
		//notifying_variable(A& a):var(a){}
		void set(A value){ // A& may be more efficient, but this is more practical in cases when a reference cannot be taken
			ul l(m);
			var=value;
			cv.notify_all();
		}
		A get(){
			ul l(m);
			return var;
		}
		void wait(){
			ul l(m);
			cv.wait(l);
		}
		A wait_and_get(){
			ul l(m);
			cv.wait(l);
			return var;
		}
	};
}

}


#endif