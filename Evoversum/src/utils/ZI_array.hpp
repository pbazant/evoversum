#ifndef ZI_ARRAY_H
#define ZI_ARRAY_H

#include <array>

namespace utils {
	//zero-initialized array
    template <class T,size_t SIZE>
	struct ZI_array: std::array<T,SIZE>{
        ZI_array() { this->fill(0);}
	};
}

#endif