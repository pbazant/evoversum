#ifndef SMART_STRUCT_HPP
#define SMART_STRUCT_HPP
#include <boost/preprocessor/repeat.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/preprocessor/seq.hpp>
#include <boost/preprocessor/cat.hpp>
#include <string>
#include "json_serialization.hpp"
#include "json_spirit_convenience.hpp"

// BOOST_FUSION_DEFINE_STRUCT does something similar, but does not support non-default member initialization

#define SMART_STRUCT_MEMBER_DECL(r,_,member) BOOST_PP_SEQ_ELEM(0,member) BOOST_PP_SEQ_ELEM(1,member);
#define SMART_STRUCT_MEMBER_INITIALIZER(s,_,member) BOOST_PP_SEQ_ELEM(1,member)(BOOST_PP_SEQ_ELEM(2,member))
#define SMART_STRUCT_BY_CREF_ARG(s,_,member) const BOOST_PP_SEQ_ELEM(0,member)& BOOST_PP_SEQ_ELEM(1,member)
#define SMART_STRUCT_BY_CREF_INITIALIZER(s,_,member) BOOST_PP_SEQ_ELEM(1,member)(BOOST_PP_SEQ_ELEM(1,member))
#define SMART_STRUCT_FROM_JSON_HELPER(s,_,member) json_serialization::from_json(json_object.at(BOOST_PP_STRINGIZE(BOOST_PP_SEQ_ELEM(1,member))),this->BOOST_PP_SEQ_ELEM(1,member));
#define SMART_STRUCT_TO_JSON_HELPER(s,_,member) json_serialization::to_json(this->BOOST_PP_SEQ_ELEM(1,member),json_object[BOOST_PP_STRINGIZE(BOOST_PP_SEQ_ELEM(1,member))]);
// todo: use variadic macros to allow commas in members
// todo: consider templated structs
#define SMART_STRUCT2(name,members,base_class)\
struct name base_class\
{   BOOST_PP_SEQ_FOR_EACH(SMART_STRUCT_MEMBER_DECL,_,members)\
    name():BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(SMART_STRUCT_MEMBER_INITIALIZER,_,members)){}\
	name(BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(SMART_STRUCT_BY_CREF_ARG,_,members))):BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(SMART_STRUCT_BY_CREF_INITIALIZER,_,members)){}\
	void load_from_json(const json_spirit::mValue& source){auto& json_object=source.get_obj();BOOST_PP_SEQ_FOR_EACH(SMART_STRUCT_FROM_JSON_HELPER,_,members)}\
    void store_to_json(json_spirit::mValue& target) const {target=json_spirit::mObject();auto& json_object=target.get_obj();BOOST_PP_SEQ_FOR_EACH(SMART_STRUCT_TO_JSON_HELPER,_,members)}\
};
#define SMART_STRUCT(name,members) SMART_STRUCT2(name,members,)

#endif
