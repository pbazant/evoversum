#ifndef RUINT_HPP
#define RUINT_HPP

#include <type_traits>
#include <boost/random.hpp>

// we use boost instead of the standard library to ensure determinism across platforms
namespace utils {
	template <class UINT, class RNG>
	UINT ruint(UINT max_val,RNG& rng){
		static_assert(std::is_unsigned<UINT>::value,"Plu!");
		return boost::random::uniform_smallint<UINT>(0,max_val)(rng);
	}
}
#endif
