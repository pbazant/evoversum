#ifndef MOD_H
#define MOD_H

namespace utils {
	//positive reminder
    inline int mod(int i, int j){
		if(j<0)
			return mod(-i,-j);
		int a=i%j;
		return (a>=0)?a:a+j;
	}
/*
	long long mod(long long i, long long j){
		if(j<0)
			return mod(-i,-j);
		int a=i%j;
		return (a>=0)?a:a+j;
	}
*/
}

#endif
