#ifndef LIMITED_UINT_H
#define LIMITED_UINT_H

#include <cstdint>
#include "json_spirit_convenience.hpp"

//todo: double-check for signedness issues

namespace utils {
    template <class, unsigned int>
    class limited_uint;
}
namespace json_serialization { // jaktoze to nefunguje, kdyz odstranim AAA a metody store_to_json a load_from_json???
    template <class C, unsigned int MAX_VALUE>
    inline void to_json_AAA(const utils::limited_uint<C,MAX_VALUE>& s,json_spirit::mValue& t){t=(std::uint64_t)s.get();}
    template <class C, unsigned int MAX_VALUE>
    inline void from_json_AAA(const json_spirit::mValue& s,utils::limited_uint<C,MAX_VALUE>& t){t.set(s.get_uint64());}
}
namespace utils {
	template <class UINT, unsigned int MAX_VALUE>
	class limited_uint {
	public:
		static_assert(std::is_unsigned<UINT>::value,"Hlo");
		static_assert(MAX_VALUE<=(UINT(-1)),"Ho!");
		limited_uint():a(0){}
		limited_uint(std::uint64_t i){this->set(i);}
		void set(std::uint64_t i){
			if(i>MAX_VALUE)
				throw std::logic_error("limited_int<>::set: value out of range");
			a=(UINT)i;
		}
		UINT get() const {return a;}
        void store_to_json(json_spirit::mValue& t) const{
            json_serialization::to_json_AAA(*this,t);
        }
        void load_from_json(const json_spirit::mValue& s){
            json_serialization::from_json_AAA(s,*this);
        }
	private:
		UINT a;
	};
}


#endif