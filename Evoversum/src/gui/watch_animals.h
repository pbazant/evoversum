#ifndef WATCH_ANIMALS_H
#define WATCH_ANIMALS_H

#include <vector>

#include <QWidget>

#include <simulator/Animal_id.hpp>

namespace Ui {
class Watch_animals;
}

class Watch_animals : public QWidget
{
    Q_OBJECT
    
public:
    explicit Watch_animals(
            //std::vector<Evoversum::Animal_id>& initial_animal_ids,
            QWidget *parent = 0);
    ~Watch_animals();
    
private:
    //std::vector<Evoversum::Animal_id> animal_ids;
public:
    Ui::Watch_animals *ui;
};

#endif // WATCH_ANIMALS_H
