#include <QApplication>
#include <QProxyStyle>
#include "mainwindow.h"

 class MyProxyStyle : public QProxyStyle
 {
   public:
     int styleHint(StyleHint hint, const QStyleOption *option = 0,
                   const QWidget *widget = 0, QStyleHintReturn *returnData = 0) const
     {
         if (hint == QStyle::SH_ToolButtonStyle)
             return Qt::ToolButtonTextUnderIcon;
         return QProxyStyle::styleHint(hint, option, widget, returnData);
     }
 };

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setStyle(new MyProxyStyle);
    MainWindow w;
    w.show();
    return a.exec();
}
