#ifndef EDIT_PARAMETERS_H
#define EDIT_PARAMETERS_H

#include <QDialog>

#ifndef Q_MOC_RUN
#include <simulator/Parameters.hpp>
#endif // Q_MOC_RUN

namespace Ui {
class Edit_parameters;
}

class Edit_parameters : public QDialog
{
    Q_OBJECT
    
public:
    explicit Edit_parameters(Evoversum::Parameters p,QWidget *parent = 0);
    ~Edit_parameters();
    Evoversum::Parameters get_edited_parameters();
private:
    Ui::Edit_parameters *ui;
    Evoversum::Parameters edited_parameters;
private slots:
    void try_ok();
};

#endif // EDIT_PARAMETERS_H
