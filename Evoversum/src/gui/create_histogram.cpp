#include "create_histogram.h"
#include "ui_create_histogram.h"

Create_histogram::Create_histogram(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Create_histogram)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnHidden(1,true);
    connect(ui->tableWidget,SIGNAL(cellClicked(int,int)),this,SLOT(example_clicked(int,int)));
}

void Create_histogram::example_clicked(int row, int){
    ui->plainTextEdit->setPlainText(ui->tableWidget->item(row,1)->text());
}

QString Create_histogram::histogram_definition(){
    return ui->plainTextEdit->toPlainText();
}
Create_histogram::~Create_histogram()
{
    delete ui;
}
