#ifndef INSPECT_ANIMALS_H
#define INSPECT_ANIMALS_H

#include <QDialog>

namespace Ui {
class Inspect_animals;
}

class Inspect_animals : public QDialog
{
    Q_OBJECT
    
public:
    explicit Inspect_animals(std::vector<std::string>& strings,QWidget *parent = 0);
    ~Inspect_animals();
    
private:
    Ui::Inspect_animals *ui;
};

#endif // INSPECT_ANIMALS_H
