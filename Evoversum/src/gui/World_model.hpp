#ifndef WORLD_MODEL_HPP
#define WORLD_MODEL_HPP

#include <memory>

#ifndef Q_MOC_RUN
#include <boost/signals2.hpp>
#endif // Q_MOC_RUN

#include <QAbstractTableModel>
#include <QSize>
#include <QObject>
#include <QModelIndex>
#include <QVariant>

#ifndef Q_MOC_RUN
#include <simulator/World_safe.hpp>
#endif // Q_MOC_RUN

class World_model : public QAbstractTableModel
{   Q_OBJECT
private:
    std::shared_ptr<Evoversum::World_safe> ws;
    boost::signals2::scoped_connection sc;
    void reset_hack(){
        beginResetModel(); //ugly
        endResetModel();
    }
public:
    World_model(std::shared_ptr<Evoversum::World_safe> ws,QObject *parent=0):
        QAbstractTableModel(parent),
        ws(ws){
        sc=ws->subscribe_changed(std::bind(&World_model::reset_hack, this));
    }
    int rowCount(const QModelIndex&/*parent = QModelIndex()*/) const {
        return ws->dimensions().y;
    }
    int columnCount(const QModelIndex&/*parent = QModelIndex()*/) const {
        return ws->dimensions().x;
    }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
        if (!index.isValid() || role != Qt::DisplayRole)
            return QVariant();
        //return QVariant::fromValue(Site_wrapper(w->site(Place(index.column(),index.row()))));
        auto pl=Evoversum::Place(index.column(),index.row());
        //return QVariant::fromValue(ws->site_state(pl)+
          //                         (ws->site_has_animal(pl)?8:0));//hack
        return QVariant::fromValue(ws->site_info_partial(pl));
    }
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const
    {   return (role == Qt::SizeHintRole)?QSize(1, 1):QVariant(); }
};

#endif // WORLD_MODEL_HPP
