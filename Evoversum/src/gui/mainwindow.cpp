#include "mainwindow.h"

#include <memory>
#include <utility>
#include <string>
#include <vector>
#include <fstream>
#include <functional>

#include <QImage>

#include <QMessageBox>
#include <QItemSelection>
#include <QInputDialog>
#include <QFileDialog>
#include <QMdiSubWindow>
#include <QRgb>

#include <utils/json_serialization.hpp>
#include <simulator/World_safe.hpp>

#include "new_world.h"
//#include "create_histogram.h"
#include "watch_animals.h"
#include "visualization.h"
#include "edit_parameters.h"

#include "ui_watch_animals.h"
#include "ui_new_world.h"

bool new_from_pixmap(Evoversum::World_safe& ws,QImage im){
    using namespace Evoversum;
    if((im.height()<2)||(im.width()<N_STATES))
        return false;
    auto animal_key=im.pixel(0,0);
    std::map<QRgb,Site_state> m;
    for(int i=0;i<N_STATES;i++)// a bit ugly -- relies on states being like 0,1,2,3...
        m[im.pixel(i,1)]=i;
    unsigned int w_e=0;
    while((1u<<w_e)<(unsigned int)im.width())
        w_e++;
    auto w=1u<<w_e;
    unsigned int h_e=0;
    while((1u<<h_e)+2<(unsigned int)(im.height()))
        h_e++;
    auto h=1u<<h_e;
    ws.new_world(Dim_exponents(w_e,h_e));
    std::vector<std::pair<Place,Site_state> >
            places_states;
    std::vector<Place> animal_places;
    for(unsigned int y=0;y<h;y++)
        for(unsigned int x=0;x<w;x++){
            auto place=Place(x,y);
            if(y+2>=(unsigned int)im.height() || x>=(unsigned int)im.width())
                places_states.push_back(std::make_pair(place,WALL));
            else {
                auto pix=im.pixel(x,y+2);
                if(m.count(pix))
                    places_states.push_back(std::make_pair(place,m[pix]));
                if(pix==animal_key)
                    animal_places.push_back(place);
            }
        }
    ws.create_primordial_animals(animal_places);
    ws.set_site_states(places_states);
    return true;
}

MainWindow::MainWindow(QWidget *parent,std::shared_ptr<Evoversum::World_safe> ws) :
    QMainWindow(parent),ws(ws),world_model(ws)
{
    ui.setupUi(this);
    sc=ws->subscribe_changed(std::bind(&MainWindow::update_statistics, this));

    connect(ui.action_new,SIGNAL(triggered()),this,SLOT(file_new()));
    connect(ui.action_open,SIGNAL(triggered()),this,SLOT(file_open()));
    connect(ui.action_save_as,SIGNAL(triggered()),this,SLOT(file_save_as()));
    connect(ui.action_save,SIGNAL(triggered()),this,SLOT(file_save()));

    timer.setSingleShot(true);
    connect(&timer,SIGNAL(timeout()),this,SLOT(evolve_world()));
    connect(ui.action_step,SIGNAL(triggered()),this,SLOT(one_step()));
    connect(ui.action_running,SIGNAL(toggled(bool)),this,SLOT(running_changed(bool)));

    connect(ui.action_seed,SIGNAL(triggered()),this,SLOT(seed_rng()));
    connect(ui.action_edit_parameters,SIGNAL(triggered()),this,SLOT(edit_parameters()));

    connect(ui.action_add_visualization,SIGNAL(triggered()),this,SLOT(add_visualization_window()));

    connect(ui.action_tabbed_view,SIGNAL(toggled(bool)),SLOT(set_tabbed(bool)));
    ws->new_world(Evoversum::Dim_exponents(7,7));
    add_visualization_window();
    ui.mdiArea->tileSubWindows();
    std::vector<Evoversum::Place> places;
    for(int i=0;i<10;i++)
        places.push_back(Evoversum::Place(i,i));
    ws->create_primordial_animals(places);
    this->resize(800,600);
    ui.dockWidget_2->hide();
}

void MainWindow::file_new(){
    New_world d;
    if(!d.exec())
        return;
    if(d.ui->new_world_mode->currentIndex().row()==0){
        if(d.ui->manual_mode_choice->currentIndex().row()==0){
            auto e_x=d.ui->width_parameter->currentIndex();
            auto e_y=d.ui->height_parameter->currentIndex();
            ws->new_world(Evoversum::Dim_exponents(e_x,e_y));
            ui.filename->setText("");
            return;
        }
        if(d.ui->manual_mode_choice->currentIndex().row()==1){
            auto file_name = QFileDialog::getOpenFileName(this, "New World From Raster Image",QString(),QString());
            if (!file_name.isEmpty()){
                QImage image;
                image.load(file_name);
                if(new_from_pixmap(*ws,image))
                    ui.filename->setText("");
            }
            return;
        }
        return;
    }
    else
        QMessageBox::information(this,"Not Implemented","This feature is not yet implemented.");

}
void MainWindow::file_open(){
    auto file_name = QFileDialog::getOpenFileName(this, "Open Simulation");
    if (!file_name.isEmpty()){
        std::ifstream f(file_name.toStdString());
        json_spirit::mValue v;
        bool res=json_spirit::read_stream(f,v);
        if(!res)
            QMessageBox::critical(this, "Error","Invalid simulation file -- JSON read error.");
        else {
            ws->from_json(v);
            //todo: catch exceptions
        }
        ui.filename->setText(file_name);
    }
}
void MainWindow::file_save_as(QString fn){
    auto file_name=fn.isEmpty()?QFileDialog::getSaveFileName(this,"Save Simulation"):fn;
    if (!file_name.isEmpty()){
        std::ofstream f(file_name.toStdString());
        json_spirit::mValue v;
        ws->to_json(v);
        json_spirit::write_stream(v,f);
        ui.filename->setText(file_name);
    }
}
void MainWindow::file_save(){
    file_save_as(ui.filename->text());
}

void MainWindow::evolve_world()
{
    bool slowdown=!ui.action_full_speed->isChecked();
    if(slowdown){
        ws->perform_round();
    } else {
        ws->evolve_with_time_limit(ui.fs_frame_duration->value());
    }
    timer.start(slowdown?ui.sm_pause_duration->value():0);
}
void MainWindow::one_step(){
    ws->perform_round();
}
void MainWindow::running_changed(bool state){
    if(state){
        evolve_world();
    }
    else {
        timer.stop();
    }
}

void MainWindow::seed_rng(){
    bool ok;
    int val=QInputDialog::getInt(this,"RNG Seed","Random number generator seed:",0,0,INT_MAX,1,&ok);
    if(ok)
        ws->rng_seed(val);
}
void MainWindow::edit_parameters(){
    Edit_parameters d(ws->parameters());
    d.exec();
    ws->set_parameters(d.get_edited_parameters());
}

void MainWindow::set_substance(QModelIndexList idxs,Evoversum::Site_state state){
    std::vector<std::pair<Evoversum::Place,Evoversum::Site_state> > places_states;
    for (int i = 0; i < idxs.size(); ++i){
        auto & tmp=idxs.at(i);
        places_states.push_back(
                    std::make_pair(
                        Evoversum::Place(tmp.column(),
                                         tmp.row()
                                         ),
                        state));
    }
    ws->set_site_states(places_states);
}
void MainWindow::fill_with_primordial_animals(QModelIndexList idxs){
    std::vector<Evoversum::Place> places;
    for (int i = 0; i < idxs.size(); ++i){
        auto & tmp=idxs.at(i);
        auto p=Evoversum::Place(tmp.column(),tmp.row());
        if(!ws->site_has_animal(p))
            places.push_back(p);
    }
    ws->create_primordial_animals(places);
}

//json_spirit::mValue val;
//ws->animal_to_json(p,val);
//auto json_str=json_spirit::write_string(val,json_spirit::single_line_arrays);
//ui_dialog.listWidget->addItem(QString::fromStdString(json_str));

void MainWindow::add_animal_watching_window(QModelIndexList indices) //todo: make it really watching instead of inspecting!!!!!!!
{
    //std::vector<Evoversum::Animal_id> animal_ids;
    auto w=new Watch_animals;
    for(int i=0;i<indices.size();i++){
        auto p=Evoversum::Place(indices[i].column(),indices[i].row());
        if(ws->site_has_animal(p))
        {
            json_spirit::mValue val;
            ws->animal_to_json(p,val);
            auto std_str=json_spirit::write_string(val,json_spirit::single_line_arrays);
            w->ui->listWidget->addItem(QString::fromStdString(std_str));
        }
    }
    auto window=ui.mdiArea->addSubWindow(w);
    window->resize(500,500);
    window->show();
}

void MainWindow::add_visualization_window(){
    auto widget=new Visualization(world_model,world_delegate,this);
    connect(widget,
            SIGNAL(set_substance_req(QModelIndexList,Evoversum::Site_state)),
            SLOT(set_substance(QModelIndexList,Evoversum::Site_state)));
    connect(widget,
            SIGNAL(fill_with_animals_req(QModelIndexList)),
            SLOT(fill_with_primordial_animals(QModelIndexList)));
    connect(widget,
            SIGNAL(animal_watching_req(QModelIndexList)),
            SLOT(add_animal_watching_window(QModelIndexList)));
    ui.mdiArea->addSubWindow(widget)->show();
}


//    auto idxs=ui.tableView->selectionModel()->selectedIndexes();
//    std::vector<Evoversum::Animal_id> ids;
//    for (int i = 0; i < idxs.size(); ++i){
//        auto & tmp=idxs.at(i);
//        auto p=Evoversum::Place(tmp.column(),tmp.row());
//        if(ws->site_has_animal(p)){
//            //ids.push_back(ws->);
//        }
//    }

void MainWindow::update_statistics(){
    ui.statistic_elapsed_steps->setText(QString::number(ws->round())); //cannot use setNum, as n_steps is int64_t
    ui.statistic_n_animals->setNum((int)ws->n_animals());
}

void MainWindow::set_tabbed(bool b){
        ui.mdiArea->setViewMode(b?QMdiArea::TabbedView:QMdiArea::SubWindowView);
}
