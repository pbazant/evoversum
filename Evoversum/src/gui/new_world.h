#ifndef NEW_WORLD_H
#define NEW_WORLD_H

#include <QDialog>

namespace Ui {
class New_world;
}

class New_world : public QDialog
{
    Q_OBJECT
public:
    explicit New_world(QWidget *parent = 0);
    ~New_world();
    
public:
    Ui::New_world *ui;
};

#endif // NEW_WORLD_H
