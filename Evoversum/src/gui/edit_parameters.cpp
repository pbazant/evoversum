#include <cctype>

#include "edit_parameters.h"
#include "ui_edit_parameters.h"

#include <QString>
#include <QMessageBox>

Edit_parameters::Edit_parameters(Evoversum::Parameters p,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Edit_parameters)
{
    ui->setupUi(this);
    json_spirit::mValue v;
    p.store_to_json(v);
    auto str=json_spirit::write_string(v,json_spirit::pretty_print);
    ui->plainTextEdit->setPlainText(QString::fromStdString(str));
    edited_parameters=p;
    connect(ui->buttonBox,SIGNAL(accepted()),SLOT(try_ok()));
}

Edit_parameters::~Edit_parameters()
{
    delete ui;
}

void Edit_parameters::try_ok(){
    std::string str=ui->plainTextEdit->toPlainText().toStdString();
    json_spirit::mValue v;
    auto sb=begin(str);
    auto se=end(str);
    if(!json_spirit::read_range(sb,se,v)){
        QMessageBox::critical(this,"Syntax error",
                              "The string entered is "
                              "not a valid JSON string. "
                              "Check for missing commas, "
                              "parentheses or quotation marks.");
        return;
    }
    for(sb;sb!=se;sb++){
        if(!isspace(*sb)){
            QMessageBox::critical(this,"Syntax error",
                                  "The string entered is "
                                  "not a valid JSON string. "
                                  "Check for extra characters "
                                  "at the end.");
            return;
        }
    }
    Evoversum::Parameters result;
    try {
        result.load_from_json(v);
    } catch(std::exception&/*e*/){
        QMessageBox::critical(this,"Structure error","Incorrect data structure.");
        //QMessageBox::critical(this,"Details:",e.what());
        return;
    }
    edited_parameters=result;
    accept();
}

Evoversum::Parameters Edit_parameters::get_edited_parameters(){
    return edited_parameters;
}
