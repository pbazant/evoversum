#include "visualization.h"
#include "ui_visualization.h"
#include "World_model.hpp"
#include "World_delegate.hpp"

#include <simulator/States.hpp>

Visualization::Visualization(World_model& model,
                             World_delegate& delegate,
                             QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Visualization)
{
    ui->setupUi(this);
    ui->visualization->setModel(&model);
    ui->visualization->setItemDelegate(&delegate);

    ui->visualization->addAction(ui->action_space);
    ui->visualization->addAction(ui->action_wall);
    ui->visualization->addAction(ui->action_food);
    ui->visualization->addAction(ui->action_diamond);
    ui->visualization->addAction(ui->action_place_primordial_animals);
    ui->visualization->addAction(ui->action_add_animal_watching_window);
    ui->visualization->addAction(ui->action_toggle_toolbar);

    connect(ui->action_space,SIGNAL(triggered()),SLOT(emit_set_to_space_req()));
    connect(ui->action_wall,SIGNAL(triggered()),SLOT(emit_set_to_wall_req()));
    connect(ui->action_food,SIGNAL(triggered()),SLOT(emit_set_to_food_req()));
    connect(ui->action_diamond,SIGNAL(triggered()),SLOT(emit_set_to_diamond_req()));
    connect(ui->action_place_primordial_animals,SIGNAL(triggered()),SLOT(emit_fill_with_animals_req()));
    connect(ui->action_add_animal_watching_window,SIGNAL(triggered()),SLOT(emit_animal_watching_req()));

    connect(ui->zoom,SIGNAL(valueChanged(int)),SLOT(set_zoom(int)));
    set_zoom(ui->zoom->value());

}

Visualization::~Visualization()
{
    delete ui;
}

void Visualization::set_zoom(int n_pixels){
    ui->visualization->horizontalHeader()->setDefaultSectionSize(n_pixels);
    ui->visualization->verticalHeader()->setDefaultSectionSize(n_pixels);
}

void Visualization::emit_set_to_space_req(){
    auto si=ui->visualization->selectionModel()->selectedIndexes();
    emit set_substance_req(si,Evoversum::SPACE);
}
void Visualization::emit_set_to_wall_req(){
    auto si=ui->visualization->selectionModel()->selectedIndexes();
    emit set_substance_req(si,Evoversum::WALL);
}
void Visualization::emit_set_to_food_req(){
    auto si=ui->visualization->selectionModel()->selectedIndexes();
    emit set_substance_req(si,Evoversum::FOOD);
}
void Visualization::emit_set_to_diamond_req(){
    auto si=ui->visualization->selectionModel()->selectedIndexes();
    emit set_substance_req(si,Evoversum::DIAMOND);
}

void Visualization::emit_fill_with_animals_req(){
    auto si=ui->visualization->selectionModel()->selectedIndexes();
    emit fill_with_animals_req(si);
}
void Visualization::emit_animal_watching_req(){
    auto si=ui->visualization->selectionModel()->selectedIndexes();
    emit animal_watching_req(si);
}

//void updateSectionSizes(){
//    int cc=ui.tableView->model()->columnCount();
//    int rc=ui.tableView->model()->rowCount();
//    int zv=ui.zoom->value();
//    for(int i=0;i<cc;i++)
//        ui.tableView->setColumnWidth(i,zv);
//    for(int i=0;i<rc;i++)
//        ui.tableView->setRowHeight(i,zv);
//}
