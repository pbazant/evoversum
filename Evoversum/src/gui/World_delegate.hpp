#ifndef WORLD_DELEGATE_HPP
#define WORLD_DELEGATE_HPP

#include <QObject>
#include <QPixmap>
#include <QAbstractItemDelegate>
#include <QPainter>
#include <QStyleOptionViewItem>
#include <QModelIndex>
#include <QStyle>
#include <QSize>

#include <iostream>

class World_delegate : public QAbstractItemDelegate
{   Q_OBJECT
public:
    std::vector<QPixmap> pixmaps_state;
    QPixmap pixmap_unknown_state;
    QPixmap pixmap_animal_l;
    World_delegate(QObject *parent = 0): QAbstractItemDelegate(parent)
    {
        pixmaps_state.push_back(QPixmap(":/substances/space.png"));
        pixmaps_state.push_back(QPixmap(":/substances/wall.png"));
        pixmaps_state.push_back(QPixmap(":/substances/food.png"));
        pixmaps_state.push_back(QPixmap(":/substances/diamond.png"));

        //pixmap_unknown_state=QPixmap(":/substances/unknown.png");
        pixmap_animal_l=QPixmap(":/animals/animal.png");
    }
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const
    {
        if (option.state & QStyle::State_Selected)
            painter->fillRect(option.rect, option.palette.highlight());
        //Site_wrapper s=index.data().value<Site_wrapper>();
        auto s=index.data().toULongLong();
        auto state=s&3;
        painter->save();
        painter->drawPixmap(option.rect, pixmaps_state[state]);
        if(s&4){
            QTransform t;
            t.rotate(-90*(int)((s>>4)&3));
            t.scale(1,((int)((s&8)>>2))-1);
            painter->drawPixmap(option.rect,pixmap_animal_l.transformed(t));
        }
        painter->restore();
    }

    QSize sizeHint(const QStyleOptionViewItem &,
                   const QModelIndex &) const {return QSize();}
};

#endif // WORLD_DELEGATE_HPP
