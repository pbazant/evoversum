#ifndef CREATE_HISTOGRAM_H
#define CREATE_HISTOGRAM_H

#include <QDialog>
#include <QString>

namespace Ui {
class Create_histogram;
}

class Create_histogram : public QDialog
{
    Q_OBJECT
    
public:
    explicit Create_histogram(QWidget *parent = 0);
    ~Create_histogram();
    QString histogram_definition();
public slots:
    void example_clicked(int row,int);
private:
    Ui::Create_histogram *ui;
};

#endif // CREATE_HISTOGRAM_H
