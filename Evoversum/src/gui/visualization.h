#ifndef VISUALIZATION_H
#define VISUALIZATION_H

#include <QWidget>
#include <QModelIndexList>

#include "World_model.hpp"
#include "World_delegate.hpp"

namespace Ui {
class Visualization;
}
class Visualization : public QWidget
{
    Q_OBJECT
public:
    explicit Visualization(World_model& model,
                           World_delegate& delegate,
                           QWidget *parent = 0);
    ~Visualization();
signals:
    void set_substance_req(QModelIndexList indices,Evoversum::Site_state);
    void fill_with_animals_req(QModelIndexList indices);
    void animal_watching_req(QModelIndexList indices);
private slots:
    void emit_set_to_space_req();
    void emit_set_to_wall_req();
    void emit_set_to_food_req();
    void emit_set_to_diamond_req();
    void emit_fill_with_animals_req();
    void emit_animal_watching_req();
    void set_zoom(int n_pixels);
private:
    Ui::Visualization *ui;
};

#endif // VISUALIZATION_H
