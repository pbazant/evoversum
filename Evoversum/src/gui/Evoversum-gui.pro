#-------------------------------------------------
#
# Project created by QtCreator 2012-02-20T20:41:44
#
#-------------------------------------------------

QT       += core widgets

TARGET = Evoversum-gui

TEMPLATE = app

SOURCES += main.cpp \
    ../simulator/World_safe.cpp \
    create_histogram.cpp \
    visualization.cpp \
    watch_animals.cpp \
    edit_parameters.cpp \
    mainwindow.cpp \
    new_world.cpp

HEADERS  += mainwindow.h \
    World_model.hpp \
    World_delegate.hpp \
    create_histogram.h \
    visualization.h \
    watch_animals.h \
    edit_parameters.h \
    new_world.h

INCLUDEPATH += ..\
    ../json_spirit_tweaked \


CONFIG+=c++11

FORMS    += mainwindow.ui \
    new_world.ui \
    create_histogram.ui \
    visualization.ui \
    watch_animals.ui \
    edit_parameters.ui

RESOURCES += \
    Resources.qrc

