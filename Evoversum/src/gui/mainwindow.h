#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>

#ifndef Q_MOC_RUN
#include <boost/signals2.hpp>
#endif // Q_MOC_RUN

#include <QMainWindow>
#include <QTimer>
#include <QString>

#include "ui_mainwindow.h"

#include "World_model.hpp"
#include "World_delegate.hpp"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0,
                        std::shared_ptr<Evoversum::World_safe> ws
                        =std::shared_ptr<Evoversum::World_safe>(new Evoversum::World_safe));
    ~MainWindow(){}
private:
    std::shared_ptr<Evoversum::World_safe> ws;
    boost::signals2::scoped_connection sc;
    World_delegate world_delegate;
    World_model world_model;
    Ui::MainWindow ui;
    QTimer timer;
private slots:
    void file_new();
    void file_open();
    void file_save_as(QString fn="");
    void file_save();

    void evolve_world();
    void one_step();
    void running_changed(bool state);

    void seed_rng();
    void edit_parameters();

    void set_substance(QModelIndexList indices,Evoversum::Site_state state);
    void fill_with_primordial_animals(QModelIndexList indices);
    void add_animal_watching_window(QModelIndexList indices);

    void add_visualization_window();

    void update_statistics();

    void set_tabbed(bool b);
};

#endif // MAINWINDOW_H
